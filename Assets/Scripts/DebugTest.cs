﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class DebugTest : MonoBehaviour
{
    //Propiedad
    public string debugString;
 
    void Awake(){
       
        Debug.Log(debugString);
 
    }
 
    void Reset()
    {
        //Output the message to the Console
        Debug.Log("Reset");
        debugString = "Hola caracula";
    }
 
    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 1);
    }
    private void OnEnable()
    {
        Debug.Log("OnEnable");
    }
    void OnDisable()
    {
        Debug.Log("OnDisable");
    }
    IEnumerator Start()
    {
        Debug.Log("Start1");
        yield return new WaitForSeconds(2.5f);
        Debug.Log("Start2");
    }
   void Update(){
       Debug.Log("Guay");
   }
}