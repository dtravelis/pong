﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float velocidadY;

    private float posy;
    public float maxY;
    public float minY;
    private float direction;
    

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        direction = Input.GetAxis("Vertical");

        posy = transform.position.y + direction*velocidadY*Time.deltaTime;
                
        if(posy >= maxY)
        {
            posy = maxY;
        }

        if(posy <= minY)
        {
            posy = minY;
        }

        transform.position = new Vector3(transform.position.x, posy, transform.position.z);
    }
}
