﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float ballSpeedY;
    public float ballSpeedX;
    private float ballPositionY;
    private float ballPositionX;
    public float maxY;
    public float minY;
    public float maxX;
    public float minX;

    // Start is called before the first frame update
    void Start()
    {
         
    }

    // Update is called once per frame
    void Update()
    {
        ballPositionY = transform.position.y + ballSpeedY*Time.deltaTime;
        ballPositionX = transform.position.x + ballSpeedX*Time.deltaTime;

        if(ballPositionY >= maxY)
        {
            ballSpeedY *= -1;;
        }
        if(ballPositionY <= minY)
        {
            ballSpeedY *= -1;
        }
        if(ballPositionX >= maxX)
        {
            ballSpeedX *= -1;;
        }
        if(ballPositionX <= minX)
        {
            ballSpeedX *= -1;
        }


        transform.position = new Vector3(ballPositionX, ballPositionY, transform.position.z);
    
    }
}
